void main() {
  String name = "Ujwal Basnet";
  int rollNo = 7;
  double percent = 32;

  List<String> hobbies = ["A", "B", "C"];

  String grade;
  // > 80 A
  // 70 - 80 B
  // 60 - 70 C
  // 50 - 60 D
  // 40 - 50 E
  // < 40 Fail
  if (percent > 80) {
    grade = "A";
  } else if (percent > 70) {
    grade = "B";
  } else if (percent > 60) {
    grade = "C";
  } else if (percent > 50) {
    grade = "D";
  } else if (percent > 40) {
    grade = "E";
  } else {
    grade = "Fail";
  }

  // for (int i = 0; i < hobbies.length; i++) {
  //   print(hobbies[i]);
  // }

  hobbies.forEach((hobby) {
    print(hobby);
  });

  int temp = 1;
  while (temp != rollNo) {
    print("$temp You are not $name");
    temp += 1; // temp = temp + 1
  }
}
