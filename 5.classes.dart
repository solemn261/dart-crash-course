class Student {
  String name;
  int rollNo;
  double percent;
  List<String> favSubs;

  Student({this.name, this.rollNo, this.percent, this.favSubs});

  generateReport() {
    print("Roll No: $rollNo");
    print("Name: $name");
    print("Percent: $percent");
  }

  @override
  String toString() {
    return "$name";
  }
}

void main() {
  List<Student> students = [
    Student(name: "Ram", rollNo: 1, percent: 50, favSubs: ["M", "P", "E"]),
    Student(name: "Sita", rollNo: 2, percent: 40, favSubs: ["M", "C", "E"]),
    Student(name: "Laxman", rollNo: 3, percent: 80, favSubs: ["M", "C", "P"]),
    Student(name: "Hari", rollNo: 4, percent: 72, favSubs: ["M", "N", "E"]),
    Student(name: "Krishna", rollNo: 5, percent: 55, favSubs: ["M", "N", "C"]),
  ];

  final result = students.where((student) {
    if (student.favSubs.contains("N")) {
      return true;
    }
    return false;
  });

  print(result);
}
