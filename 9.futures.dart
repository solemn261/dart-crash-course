Future<List<String>> getStudents() {
  return Future.delayed(Duration(seconds: 2), () {
    return Future.error("Data fetching failed.");
    // return ["Ram", "Sita", "Hari", "Krishna"];
  });
}

void main() async {
  // getStudents().then((data) {
  //   print("Success");
  //   print(data);
  // }).catchError((error) {
  //   print("Failure");
  //   print(error);
  // });

  try {
    final result = await getStudents();
    print(result);
  } catch (e) {
    print("Error $e");
  }
}

class Failure {}
