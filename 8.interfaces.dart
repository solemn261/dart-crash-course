abstract class Shape {
  double area();
}

class Rectangle implements Shape {
  double length;
  double breadth;

  Rectangle(this.length, this.breadth);

  @override
  double area() => length * breadth;
}

void main() {
  Shape rect = Rectangle(5, 6);
  print(rect.area());
}
