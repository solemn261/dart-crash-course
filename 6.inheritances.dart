class Box {
  double length;
  double breadth;
  double height;

  double get volume => length * breadth * height;

  Box(this.length, this.breadth, this.height);
}

class Cube extends Box {
  double side;
  Cube(this.side) : super(side, side, side);
}

void main() {
  Cube cube = Cube(2);
  print(cube.volume);
}
