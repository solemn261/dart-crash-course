mixin Student {
  String name;
  int sId;

  printSInfo() => print("S$sId. $name");
}

mixin Teacher {
  String name;
  int tId;

  printTInfo() => print("T$tId. $name");
}

class StudentAndTeacher with Teacher, Student {
  StudentAndTeacher(String name, int sid, int tid) {
    this.name = name;
    this.sId = sid;
    this.tId = tid;
  }
}

void main() {
  StudentAndTeacher s = StudentAndTeacher("Ram", 1, 5);
  s.printSInfo();
  s.printTInfo();
}
