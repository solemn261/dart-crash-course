Stream<String> subscribeChannel(String userName, String channelName) async* {
  yield "Hello $userName, You have subscribed to $channelName";
  await Future.delayed(Duration(seconds: 2));
  yield "Flutter UI Challange - Fitness UI - Day1 has been uploaded";
  await Future.delayed(Duration(seconds: 2));
  yield "Dart Crash course has been uploaded";
  await Future.delayed(Duration(seconds: 2));
  yield "Flutter Crash course has been uploaded";
}

void main() {
  subscribeChannel("Ram", "Solemn").listen((data) {
    print(data);
  });
}
