void main() {
  var x = 50.0;

  // Student detail
  String name = "Ujwal Basnet";
  int rollNo = 7;
  double percent = 50;
  List<String> hobbies = ["A", "B", "C"];

  print(
      "Hello $name, Your roll no is: $rollNo, You have scored $percent%. And your hobbies are $hobbies");
}
